package com.experis;

import org.json.*;
import java.io.*;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.*;

public class Main {
    private final String CHARACTERS = "characters/";
    private final String BOOKS = "books/";
    private final String HOUSES = "houses/";

    /**
     * Fetches the house with the given id and stores all of
     * its sworn members in an JSONArray and forwards the object
     * to be printed
     * @param houseId id of the house which to print characters
     */
    public void printHouseCharacters(String houseId) {
        JSONObject jsonObject = (JSONObject) getFromAPI(HOUSES + houseId);
        JSONArray jsonArray = jsonObject.getJSONArray("swornMembers");
        printCharacterName(jsonArray);
    }

    /**
     * Prints the name of all of the characters from the JSONArray
     * @param jsonArray the JSONArray containing JSONObjects of characters
     */
    public void printCharacterName(JSONArray jsonArray) {

        //Since the API gives the whole URL to the character,
        //I extract the character id so that I can reuse getFromAPI(String)
        String id="";
        for (Object obj : jsonArray) {
            id = obj.toString();
            id= id.substring(id.lastIndexOf("/")+1);
            System.out.println(getCharacterName(id));
        }
    }

    /**
     * Gets the character with the given id in JSONObject and returns the name of the character
     * @param id the id of the character to get the name for
     * @return the name of the character
     */
    public String getCharacterName(String id) {
        JSONObject character = (JSONObject) getFromAPI(CHARACTERS + id);
        return character.get("name").toString();
    }

    /**
     * Extracts basic information of the character with the given id and prints it out.
     * Also returns the id of the (first) house/allegiance which the character belongs to.
     * @param id the id of the character to print info about
     * @return the id of the characters house
     */
    public String printCharacterInfo(String id) {
        JSONObject jsonObject = (JSONObject) getFromAPI(CHARACTERS + id);
        if(jsonObject==null) {
            System.out.println("No person found. Try another number");
            return "";
        } else {
            String name = jsonObject.get("name").toString();
            String gender = jsonObject.get("gender").toString();
            String culture = jsonObject.get("culture").toString();
            JSONArray aliasArray = jsonObject.getJSONArray("aliases");
            String aliases = "";
            // if aliasArray is [""], it means that the character does not belong
            // to any allegiances.
            if(!aliasArray.toString().equals("[\"\"]")) {
                for (Object obj: aliasArray ) {
                    aliases+= obj.toString() + ", ";
                }
                aliases.substring(0, aliases.length()-2);
            }

            StringBuilder builder = new StringBuilder();
            builder.append("Name: ").append(name).append("\n");
            builder.append("Gender: ").append(gender).append("\n");
            builder.append("Culture: ").append(culture).append("\n");
            builder.append("Aliases: ").append(aliases).append("\n");
            System.out.println(builder);
            String house = jsonObject.get("allegiances").toString(); // gets only the first allegiance
            house = house.substring(house.lastIndexOf("/")+1, house.length()-2);
            //System.out.println("House id: " + house);
            return house;
        }
    }

    /**
     * Fetches all of the books and prints out the title of the ones that are published by Bantam Books
     * along with the pov characters of the books
     */
    public void printPOVCharacters() {
        JSONArray jsonArray = (JSONArray) getFromAPI(BOOKS);

        HashMap<String, JSONArray> map = new HashMap<>();
        JSONObject jsonObject;
        String publisher="";
        for(int i =0; i<jsonArray.length()-1; i++) {
            jsonObject = jsonArray.getJSONObject(i);
            publisher = jsonObject.getString("publisher");
            if(publisher.equals("Bantam Books")) {
                map.put(jsonObject.getString("name"), jsonObject.getJSONArray("povCharacters"));
            }
        }

        Iterator it = map.entrySet().iterator();
        final String ANSI_YELLOW = "\u001B[33m";
        final String ANSI_RESET = "\u001B[0m";

        System.out.println("---------- BOOKS PUBLISHED BY BANTAM BOOKS & POV CHARACTERS ------------");
        while (it.hasNext()) {
            Map.Entry pair = (Map.Entry)it.next();
            System.out.println(ANSI_YELLOW + "Book title: " + ANSI_RESET + pair.getKey());
            printCharacterName((JSONArray) pair.getValue());
            System.out.println();
            System.out.println();
            it.remove();
        }
    }

    /**
     * Connects to the API and requests some information depending on what is given.
     * Then returns an JSONObject or JSONArray, depending on what is being returned by
     * the API.
     * @param id what is being requested.
     * @return a JSONObject or JSONArray with some content
     */
    public Object getFromAPI(String id) {
        String urlStr= "https://anapioficeandfire.com/api/" + id;
        //System.out.println("Call to: " + urlStr);
        Object json = null;
        try {
            URL url = new URL(urlStr);
            HttpURLConnection con = (HttpURLConnection) url.openConnection();
            con.setRequestMethod("GET");

            if(con.getResponseCode() == HttpURLConnection.HTTP_OK) {

                InputStreamReader isr = new InputStreamReader(con.getInputStream());
                BufferedReader br = new BufferedReader(isr);
                String inputLine;
                StringBuffer content = new StringBuffer();

                while ((inputLine = br.readLine()) != null) {
                    content.append(inputLine);
                }
                br.close();

                if(content.charAt(0) =='['){
                    json = new JSONArray(content.toString());
                } else {
                    json = new JSONObject(content.toString());
                }

            }
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return json;
    }

    /**
     * Checks to see if the given input contains only numbers.
     * @param input the input to check
     * @return true if only numbers, else false
     */
    private boolean correctInput(String input) {
        String regex = "[0-9]+";
        if(input.matches(regex)){
            return true;
        }
        System.out.println("Incorrect input. Please enter a number");
        return false;
    }

    /**
     * Prompts the user to input a number depending on what the given opt is.
     * For example, if opt=0, then the prompt will be to enter a character id.
     * If opt=1, then the prompt will be to decide whether or not to see
     * the character's house members.
     * @param opt option for type of input to ask for
     * @return the user's input
     */
    public String promptUserInput(int opt) {
        String str = "";
        switch (opt) {
            case 0 :
                str = "Enter the number of the character you wish to get info about: ";
                break;
            case 1 :
                str = "Do you wish to see the list of sworn members for this character's house?" +
                        "\nEnter 1 for \"yes\", or any other key to exit";
                break;

        }
        System.out.println(str);
        Scanner scanner = new Scanner(System.in);
        return scanner.next();
    }

    public static void main(String[] args) {
        Main main = new Main();

        String input = "";
        do {
            input = main.promptUserInput(0);
        } while (!main.correctInput(input));
        String houseId = main.printCharacterInfo(input);
        if(houseId.isEmpty()) {} // if no houses are found, don't ask to view the house's members
        else {
            input = main.promptUserInput(1);

            if(Integer.parseInt(input)==1) {
                main.printHouseCharacters(houseId);
                main.printPOVCharacters();
            }
        }

    }

}